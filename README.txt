CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers

INTRODUCTION
------------

The module provide you Manually Add multiple cron time in the second.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/cron_timing

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/cron_timing


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Cron Timing module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.


CONFIGURATION
-------------

 * The Module have only small configuration form there add timing of corn as
 per your requirements (/admin/config/system/cron_timing)


USAGE
-----

 * Using the cron timing module you can Manually Add multiple cron time in the
 second (admin/config/system/cron_timing).
 * After Set Cron Configuration form (/admin/config/system/cron) get this all
 timing value in (min, hr, day) formate


MAINTAINERS
-----------

 * Omprakash Mankar (omrmankar)- https://www.drupal.org/u/omrmankar
