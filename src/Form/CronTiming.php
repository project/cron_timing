<?php

namespace Drupal\cron_timing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The CronTiming class.
 */
class CronTiming extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cron_timing.crontiming',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cron_timing';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cron_timing.crontiming');
    $form['cron_timing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cron Timing'),
      '#description' => $this->t('Enter a value like e.g 60,120,360,900.(In second)'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('cron_timing'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!preg_match('/^[0-9]+(,[0-9]+)*$/', $form_state->getValue('cron_timing'))) {
      $form_state->setErrorByName('cron_timing', $this->t('The Cron Timing Must be In Nummeric value like e.g 60,120,360,900.(In Comma)'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cron_timing.crontiming')
      ->set('cron_timing', $form_state->getValue('cron_timing'))
      ->save();
  }

}
